import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user/user.service';
import { MessageService } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  right_button_text = "login"
  right_button_type = "success"

  constructor(public router: Router, public userService: UserService, private messageService: MessageService) {
    this.user_auth_switch()

    var self = this
    this.messageService.getMessage().subscribe(msg => {
      if (msg.text == "auth"){
        self.user_auth_switch()
      }
    })
  }
  ngOnInit(): void {
    this.user_auth_switch()
  }

  public user_auth_switch(){
    if (this.userService.isAuth){
      this.user_logout()
    }else{
      this.user_login()
    }
  }

  public login(){
    this.router.navigate(["user/login"])
  }

  public user_logout(){
    this.right_button_text = "logout"
    this.right_button_type = "danger"
  }

  public user_login() {
    this.right_button_text = "login"
    this.right_button_type = "success"
  }

  public logout() {
    this.userService.logout();
    this.user_auth_switch();
  }
}
