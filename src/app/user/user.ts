export class User {}

export class Bearer {
  // tslint:disable-next-line: variable-name
  token_type: string;
  // tslint:disable-next-line: variable-name
  access_token: string;
  // tslint:disable-next-line: variable-name
  refresh_token: string;
  username: string;
}

export interface CodeDeliveryDetails {
  Destination: string;
  DeliveryMedium: string;
  AttributeName: string;
}

export interface HTTPHeaders {
  date: string;
  connection: string;
  'content-type': string;
  'content-length': string;
  'x-amzn-requestid': string;
}

export interface ResponseMetadata {
  RequestId: string;
  HTTPStatusCode: number;
  HTTPHeaders: HTTPHeaders;
  RetryAttempts: number;
}

export interface Register {
  UserConfirmed: boolean;
  CodeDeliveryDetails: CodeDeliveryDetails;
  UserSub: string;
  ResponseMetadata: ResponseMetadata;
}
