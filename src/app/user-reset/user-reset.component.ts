import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-user-reset',
  templateUrl: './user-reset.component.html',
  styleUrls: ['./user-reset.component.scss'],
})
export class UserResetComponent implements OnInit {
  formControlUsername = new FormControl('');
  formControlCode = new FormControl('');
  formControlPassword = new FormControl('');

  constructor(
    private userService: UserService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {}

  forget(): void {
    let username: string;

    username = this.formControlUsername.value;

    const tReset = this.toastrService.show(
      'Checking Request...',
      'Reset Password',
      {
        status: 'info',
      }
    );
    const sReset = this.userService.reset(username);
    sReset.subscribe(
      (x) => {
        tReset.close();
        this.toastrService.show('Check your mails...', 'Request Send', {
          status: 'success',
        });
      },
      (err: Error) => {
        tReset.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }

  confirm(): void {
    let username: string;
    let code: string;
    let password: string;

    username = this.formControlUsername.value;
    code = this.formControlCode.value;
    password = this.formControlPassword.value;

    const tConfirm = this.toastrService.show(
      'Checking Request...',
      'New Password',
      {
        status: 'info',
      }
    );
    const sConfirm = this.userService.setPassword(username, password, code);
    sConfirm.subscribe(
      (x) => {
        tConfirm.close();
        this.toastrService.show('New Password set...', 'Success', {
          status: 'success',
        });
      },
      (err: Error) => {
        tConfirm.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }
}
