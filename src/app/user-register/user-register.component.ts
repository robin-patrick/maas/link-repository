import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserService } from '../user/user.service';
import { NbToastrService } from '@nebular/theme';
import { Register } from '../user/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss'],
})
export class UserRegisterComponent implements OnInit {
  formControlEmail = new FormControl('');
  formControlUsername = new FormControl('');
  formControlPassword = new FormControl('');

  constructor(
    private router: Router,
    private userService: UserService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {}

  register(): void {
    let username: string;
    let email: string;
    let password: string;

    username = this.formControlUsername.value;
    email = this.formControlEmail.value;
    password = this.formControlPassword.value;

    const tRegister = this.toastrService.show('Checking Request...', 'Register', {
      status: 'info',
    });
    const sRegister = this.userService.register(email, username, password);
    sRegister.subscribe(
      (x: Register) => {
        tRegister.close();
        this.toastrService.show('Check your mails', 'Success', {
          status: 'success',
        });
        this.router.navigate(['/user/confirm']);
      },
      (err: Error) => {
        tRegister.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }
}
