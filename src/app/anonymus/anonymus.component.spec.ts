import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymusComponent } from './anonymus.component';

describe('AnonymusComponent', () => {
  let component: AnonymusComponent;
  let fixture: ComponentFixture<AnonymusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnonymusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
