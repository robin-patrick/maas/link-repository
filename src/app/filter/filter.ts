export class Filter {
  field: string;
  values: Value[];
}

export class Value {
  key: string;
  // tslint:disable-next-line: variable-name
  doc_count: number;
}

export class CurrentFilter {
  host: string;
  category: string;
  tags: string;
  // tslint:disable-next-line: variable-name
  file_type: string;
  author: string;
}
