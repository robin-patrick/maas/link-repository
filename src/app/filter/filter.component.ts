import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef, NbCalendarRange } from '@nebular/theme';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { EntryService } from '../entry/entry.service';
import { CurrentFilter } from './filter';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  @Input() title: string;

  // Filter Fields
  @Input() range: NbCalendarRange<Date>;
  @Input() votes: number;
  @Input() shares: number;
  @Input() comments: number;
  @Input() fType: string;
  @Input() tag: string;
  @Input() hostname: string;
  @Input() category: string;
  @Input() author: string;
  @Input() text: string;

  // AutoComplete List
  filetypeList: string[];
  tagList: string[];
  hostnameList: string[];
  categoryList: string[];
  authorList: string[];

  // Form Control
  hostnameFormControl = new FormControl();
  categoryFormControl = new FormControl();
  tagFormControl = new FormControl();
  filetypeFormControl = new FormControl();
  authorFormControl = new FormControl();

  // Filtered Lists
  filteredHostnames$: Observable<string[]>;
  filteredCategories$: Observable<string[]>;
  filteredTags$: Observable<string[]>;
  filteredFileTypes$: Observable<string[]>;
  filteredAuthors$: Observable<string[]>;

  // Constructor
  constructor(
    protected ref: NbDialogRef<FilterComponent>,
    private entryService: EntryService
  ) {}

  // Inital
  ngOnInit(): void {
    Promise.resolve(this.entryService.filter('host', 0, 100).then((x) => {
      this.hostnameList = x;
      this.filteredHostnames$ = this.hostnameFormControl.valueChanges.pipe(
        startWith(''),
        map((f: string) => this.hostnameList.filter(v => v.toLowerCase().includes(f.toLowerCase())))
      );
    }));

    Promise.resolve(this.entryService.filter('category', 0, 100).then((x) => {
      this.categoryList = x;
      this.filteredCategories$ = this.categoryFormControl.valueChanges.pipe(
        startWith(''),
        map((f: string) => this.categoryList.filter(v => v.toLowerCase().includes(f.toLowerCase())))
      );
    }));

    Promise.resolve(this.entryService.filter('tags', 0, 100).then((x) => {
      this.tagList = x;
      this.filteredTags$ = this.tagFormControl.valueChanges.pipe(
        startWith(''),
        map((f: string) => this.tagList.filter(v => v.toLowerCase().includes(f.toLowerCase())))
      );
    }));

    Promise.resolve(this.entryService.filter('file_type', 0, 100).then((x) => {
      this.filetypeList = x;
      this.filteredFileTypes$ = this.filetypeFormControl.valueChanges.pipe(
        startWith(''),
        map((f: string) => this.filetypeList.filter(v => v.toLowerCase().includes(f.toLowerCase())))
      );
    }));

    Promise.resolve(this.entryService.filter('author', 0, 100).then((x) => {
      this.authorList = x;
      this.filteredAuthors$ = this.authorFormControl.valueChanges.pipe(
        startWith(''),
        map((f: string) => this.authorList.filter(v => v.toLowerCase().includes(f.toLowerCase())))
      );
    }));
  }

  dismiss() {
    const cf = new CurrentFilter();
    cf.category = this.categoryFormControl.value;
    cf.file_type = this.filetypeFormControl.value;
    cf.host = this.hostnameFormControl.value;
    cf.tags = this.tagFormControl.value;
    cf.author = this.authorFormControl.value;
    this.entryService.setFilter(cf);
    this.ref.close();
  }
}
