export class Comment {
  text: string;
  author: string;
  votes: number;
}

export class Meme {
  // tslint:disable-next-line: variable-name
  meme_id: string; // Internal Database ID of the Link
  url: string; // Full Link of the Entry
  host: string; // Host is the Hostname of the Entry
  created: Date;
  author: string; // Author or Username of the Entry
  title: string; // Title of the link
  text: string; // Fulltext of the link
  comments: Comment[];
  votes: number; // Karma, Likes, Hearts or whatever this site uses
  shares: number; // Shared, Retweeted number of whatever this site uses
  // tslint:disable-next-line: variable-name
  image_url: string;
  // tslint:disable-next-line: variable-name
  image_data: string;
  category: string; // Category, Subreddit, ect.
  // tslint:disable-next-line: variable-name
  file_type: string; // Type of the Entry like Video, Text, Image or Link if unknown
  tags: string[]; // Original Tags, Hashtags, Flares of the Entry
  // ocr: string[]; // Text discovered through ORC on this Entry
}

export class Entry {
  count: number;
  limit: number;
  offset: number;
  memes: Meme[];
}

// Filter
export class Term {
  host: string;
  // tslint:disable-next-line: variable-name
  file_type: string;
  category: string;
  tags: string;
  author: string;
}

export class Operators {
  gte: string;
  gt: string;
  eq: string;
  lte: string;
  lt: string;
}

export class Range {
  votes: Operators;
  shares: Operators;
  created: Operators;
}

export class Should {
  term: Term;
  bool: Bool;
}

export class Must {
  term: Term;
  bool: Bool;
}

export class Bool {
  must: Must[];
  should: Should[];
}

export class FilterSyntax {
  bool: Bool;
}
