import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Entry, FilterSyntax, Term, Must, Should, Bool } from './entry';
import { UserService } from '../user/user.service';
import { environment } from 'src/environments/environment';
import { Filter, CurrentFilter } from '../filter/filter';

@Injectable({
  providedIn: 'root',
})
export class EntryService {
  private currentFilter: CurrentFilter;

  constructor(private http: HttpClient, private userService: UserService) {
    this.currentFilter = null;
  }

  private _addTerm(fs: FilterSyntax, t: Term) {
    const s = new Should();
    s.term = t;
    const b = new Bool();
    b.should = new Array();
    b.should.push(s);
    const m = new Must();
    m.bool = b;
    fs.bool.must.push(m);
  }

  public getFilter(): string {
    if (this.currentFilter === null) {
      return 'null';
    }
    // {"bool":
    //   {"must": [
    //     {"bool":{"should":[{"term":{"host":"hugelol.com"}},{"term":{"host":"g.redditmedia.com"}}]}},
    //     {"bool":{"should":[{"term":{"file_type":"Image"}},{"term":{"file_type":"Gif"}}]}},
    //     {"bool":{"should":[{"range":{"votes":{"gte": 0,"lt": 51}}}]}}
    //   ]}
    // }
    const fs = new FilterSyntax();
    fs.bool = new Bool();
    fs.bool.must = new Array();
    console.log({...fs.bool.must});
    if (this.currentFilter.category !== null && this.currentFilter.category !== undefined) {
      const t = new Term();
      t.category = this.currentFilter.category;
      this._addTerm(fs, t);
    }
    if (this.currentFilter.file_type !== null && this.currentFilter.file_type !== undefined) {
      const t = new Term();
      t.file_type = this.currentFilter.file_type;
      this._addTerm(fs, t);
    }
    if (this.currentFilter.host !== null && this.currentFilter.host !== undefined) {
      const t = new Term();
      t.host = this.currentFilter.host;
      this._addTerm(fs, t);
    }
    if (this.currentFilter.tags !== null && this.currentFilter.tags !== undefined) {
      const t = new Term();
      t.tags = this.currentFilter.tags;
      this._addTerm(fs, t);
    }
    if (this.currentFilter.author !== null && this.currentFilter.author !== undefined) {
      const t = new Term();
      t.author = this.currentFilter.author;
      this._addTerm(fs, t);
    }
    return JSON.stringify(fs);
  }

  public search(
    query: string,
    offset: number,
    limit: number
  ): Observable<Entry> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.userService.token.access_token,
    });
    const filter = this.getFilter();
    const apiURL = `${environment.api}/api/search?query=${query}&cachable_filter=${filter}&query_type=free_text&offset=${offset}&limit=${limit}`;
    return this.http.get<Entry>(apiURL, { headers });
  }

  public addMeme(url: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.userService.token.access_token,
    });
    const body = {
      url
    };
    console.log(body);
    const apiURL = `${environment.api}/api/meme`;
    return this.http.post<any>(apiURL, body, { headers });
  }

  public setFilter(cf: CurrentFilter) {
    this.currentFilter = cf;
  }

  public findSimilar(
    meme: string,
    query: string,
    offset: number,
    limit: number
  ): Observable<Entry> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.userService.token.access_token,
    });
    const apiURL = `${environment.api}/api/similar/${meme}?query=${query}&query_type=free_text&offset=${offset}&limit=${limit}`;
    return this.http.get<Entry>(apiURL, { headers });
  }

  public deleteMeme(meme: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.userService.token.access_token,
    });
    const apiURL = `${environment.api}/api/meme/${meme}`;
    return this.http.delete<any>(apiURL, { headers });
  }

  public filter(
    field: string,
    offset: number,
    limit: number
  ): Promise<string[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.userService.token.access_token,
    });
    const apiURL = `${environment.api}/api/get_search_filter?field=${field}&query_type=free_text&offset=${offset}&limit=${limit}`;
    return this.http
      .get<Filter[]>(apiURL, { headers })
      .toPromise()
      .then((x) => {
        return x[0].values.map((y) => {
          return y.key;
        });
      });
  }
}
