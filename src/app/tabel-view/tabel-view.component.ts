import { Component, OnInit } from '@angular/core';
import { EntryService } from '../entry/entry.service';
import { Meme } from '../entry/entry';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { FilterComponent } from '../filter/filter.component';
import { AddMemeComponent } from '../add-meme/add-meme.component';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-tabel-view',
  templateUrl: './tabel-view.component.html',
  styleUrls: ['./tabel-view.component.scss'],
})
export class TabelViewComponent implements OnInit {
  constructor(
    private entry: EntryService,
    private userService: UserService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService
  ) {}

  public results: Meme[];
  public loading: boolean;
  public similar: string;

  offset: number;
  limit: number;

  ngOnInit(): void {
    this.similar = null;
    if (this.userService.token.access_token) {
      console.log(this.userService.token.access_token);
    } else {
      console.log(this.userService.token);
    }
    this.showRecent();
  }

  initMemeList(): void {
    this.loading = false;
    this.offset = 0;
    this.limit = 2;
    this.results = [];
  }

  showRecent() {
    this.similar = null;
    this.entry.setFilter(null);
    this.initMemeList();
    this.loadNext();
  }

  openFilter() {
    this.dialogService.open(FilterComponent, {}).onClose.subscribe(() => {
      this.initMemeList();
    });
  }

  openAdd() {
    this.dialogService.open(AddMemeComponent, {});
  }

  findSimilar(id: string) {
    this.initMemeList();
    this.similar = id;
    this.loading = true;
    this.entry
      .findSimilar(id, '*', this.offset, this.limit)
      .subscribe((data) => {
        this.loading = false;
        console.log(data);
        this.results = data.memes;
      });
  }

  removeMeme(id: string) {
    this.entry.deleteMeme(id).subscribe((x) => {
      const rem = this.results.find((e) => e.meme_id === id);
      const index = this.results.indexOf(rem, 0);
      if (index > -1) {
        this.results.splice(index, 1);
      }
      this.toastrService.show('Meme deleted!', 'Success', {
        status: 'success',
      });
    });
  }

  loadNext() {
    console.log([this.loading, this.offset]);
    if (!this.loading) {
      this.loading = true;
      if (this.similar === null) {
        this.entry.search('*', this.offset, this.limit).subscribe((data) => {
          if (data.memes.length > 0) {
            this.results = this.results.concat(data.memes);
            this.loading = false;
            this.offset += this.limit;
          }
        });
      } else {
        this.entry
          .findSimilar(this.similar, '*', this.offset, this.limit)
          .subscribe((data) => {
            if (data.memes.length > 0) {
              this.results = this.results.concat(data.memes);
              this.loading = false;
              this.offset += this.limit;
            }
          });
      }
    }
  }
}
