import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  NbThemeModule,
  NbIconModule,
  NbLayoutModule,
  NbActionsModule,
  NbSidebarModule,
  NbMenuModule,
  NbToastrModule,
  NbRouteTabsetModule,
  NbCardModule,
  NbButtonModule,
  NbStepperModule,
  NbListModule,
  NbAlertModule,
  NbDialogModule,
  NbBadgeModule,
  NbAccordionModule,
  NbTabsetModule,
  NbInputModule,
  NbFormFieldModule,
  NbCheckboxModule,
  NbAutocompleteModule,
  NbProgressBarModule,
  NbCalendarModule,
  NbCalendarRangeModule,
  NbTooltipModule,
  NbContextMenuModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import {
  NgcCookieConsentModule,
  NgcCookieConsentConfig,
} from 'ngx-cookieconsent';
import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabelViewComponent } from './tabel-view/tabel-view.component';
import { AnonymusComponent } from './anonymus/anonymus.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { EntryComponent } from './entry/entry.component';
import { TosComponent } from './tos/tos.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { UserComponent } from './user/user.component';
import { FilterComponent } from './filter/filter.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserConfirmComponent } from './user-confirm/user-confirm.component';
import { UserResetComponent } from './user-reset/user-reset.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { AddMemeComponent } from './add-meme/add-meme.component';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: environment.url,
  },
  palette: {
    popup: {
      background: '#000',
    },
    button: {
      background: '#f1d600',
    },
  },
  theme: 'edgeless',
  type: 'opt-out',
};

@NgModule({
  declarations: [
    AppComponent,
    TabelViewComponent,
    AnonymusComponent,
    NotFoundComponent,
    EntryComponent,
    TosComponent,
    PrivacyComponent,
    UserComponent,
    FilterComponent,
    UserLoginComponent,
    UserRegisterComponent,
    UserConfirmComponent,
    UserResetComponent,
    UserManageComponent,
    AddMemeComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot({}),
    NbCardModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbActionsModule,
    NbIconModule,
    NbRouteTabsetModule,
    NbMenuModule,
    NbButtonModule,
    NbAlertModule,
    NbStepperModule,
    NbListModule,
    NbBadgeModule,
    NbAccordionModule,
    NbTabsetModule,
    NbInputModule,
    NbFormFieldModule,
    NbCheckboxModule,
    NbAutocompleteModule,
    NbProgressBarModule,
    NbCalendarModule,
    NbCalendarRangeModule,
    NbTooltipModule,
    NbContextMenuModule,
    NgcCookieConsentModule.forRoot(cookieConfig),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [ CookieService ],
  bootstrap: [ AppComponent ],
})
export class AppModule {}
