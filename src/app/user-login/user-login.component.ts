import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserService } from '../user/user.service';
import { NbToastrService } from '@nebular/theme';
import { Bearer } from '../user/user';
import { Router } from '@angular/router';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit {
  formControlEmail = new FormControl('');
  formControlPassword = new FormControl('');

  constructor(
    private router: Router,
    private userService: UserService,
    private toastrService: NbToastrService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  login(): void {
    let email: string;
    let password: string;

    email = this.formControlEmail.value;
    password = this.formControlPassword.value;

    const tLogin = this.toastrService.show('Logging in...', 'Login', {
      status: 'info',
    });
    const sLogin = this.userService.login(email, password);
    sLogin.subscribe(
      (x: Bearer) => {
        tLogin.close();
        this.toastrService.show('Logged in!', 'Success', {
          status: 'success',
        });
        this.messageService.sendMessage("auth", {"success": true})
        this.router.navigate(['/view']);
      },
      (err: Error) => {
        tLogin.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
        this.messageService.sendMessage("auth", {"success": false})
      }
    );
  }
}
