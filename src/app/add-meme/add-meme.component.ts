import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { EntryService } from '../entry/entry.service';

@Component({
  selector: 'app-add-meme',
  templateUrl: './add-meme.component.html',
  styleUrls: ['./add-meme.component.scss'],
})
export class AddMemeComponent implements OnInit {
  formControlMeme = new FormControl('');

  constructor(
    private entryService: EntryService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {}

  private addMeme(meme: string) {
    const tMeme = this.toastrService.show('Submitting Meme...', 'Meme', {
      status: 'info',
    });
    const sMeme = this.entryService.addMeme(meme);
    sMeme.subscribe(
      (x: any) => {
        tMeme.close();
        this.toastrService.show('Meme submitted!', 'Success', {
          status: 'success',
        });
      },
      (err: Error) => {
        tMeme.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }

  submit(): void {
    let meme: string;

    meme = this.formControlMeme.value;
    let timeout = 0;
    meme.split('\n').forEach((m) => {
      setTimeout(() => {
        this.addMeme(m);
      }, timeout * 350);
      timeout += 1;
    });
  }
}
