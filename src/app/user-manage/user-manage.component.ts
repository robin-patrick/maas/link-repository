import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss'],
})
export class UserManageComponent implements OnInit {
  formControlPassword = new FormControl('');
  formControlPasswordNew = new FormControl('');

  constructor(
    private userService: UserService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {}

  manage(): void {
    let password: string;
    let passwordNew: string;

    password = this.formControlPassword.value;
    passwordNew = this.formControlPasswordNew.value;

    const tManage = this.toastrService.show(
      'Checking Request...',
      'Password Change',
      {
        status: 'info',
      }
    );
    const sManage = this.userService.manage(password, passwordNew);
    sManage.subscribe(
      (x) => {
        tManage.close();
        this.toastrService.show('Login with you new Password...', 'Password Changed', {
          status: 'success',
        });
      },
      (err: Error) => {
        tManage.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }

}
