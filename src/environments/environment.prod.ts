export const environment = {
  production: true,
  url: 'https://www.mymemelist.com',
  api: 'https://api.mymemelist.com',
};
